package com.aldes.jcs.core.entities;

import models.TexturedModel;

import org.lwjgl.util.vector.Vector3f;

import com.aldes.jcs.core.Entity;

public class GameObject extends Entity {

	public GameObject(TexturedModel model, int index, Vector3f position,
			float rotX, float rotY, float rotZ, float scale) {
		super(model, index, position, rotX, rotY, rotZ, scale);
	}
	
}
