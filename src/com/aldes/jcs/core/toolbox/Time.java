package com.aldes.jcs.core.toolbox;

public class Time {
	
	private static long lastFrameTime;
	public static float delta;
	
	
	public static long getLastFrameTime() {
		return lastFrameTime;
	}
	
	public static void setLastFrameTime(final long time) {
		lastFrameTime = time;
	}
}
