package com.aldes.jcs.managers;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;

import com.aldes.jcs.core.toolbox.Time;

public class DisplayManager {

	private static final int FPS_CAP = 120;
	
	private static int SCREEN_WIDTH = 800;
	private static int SCREEN_HEIGHT = 600;
	private static String SCREEN_TITLE = "JCSJ_Framework Default Title";
	
	public static void createDisplay() {
		ContextAttribs attribs = new ContextAttribs(3,2)
		.withForwardCompatible(true)
		.withProfileCore(true);
		
		try {
			Display.setDisplayMode(new DisplayMode(SCREEN_WIDTH, SCREEN_HEIGHT));
			Display.create(new PixelFormat(), attribs);
			Display.setTitle(SCREEN_TITLE);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		
		GL11.glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		Time.setLastFrameTime(getCurrentTime());
	}
	
	public static void createDisplay(int width, int height, String title) {
		SCREEN_WIDTH = width;
		SCREEN_HEIGHT = height;
		SCREEN_TITLE = title;

		ContextAttribs attribs = new ContextAttribs(3,2)
		.withForwardCompatible(true)
		.withProfileCore(true);
		
		try {
			Display.setDisplayMode(new DisplayMode(SCREEN_WIDTH, SCREEN_HEIGHT));
			Display.create(new PixelFormat(), attribs);
			Display.setTitle(SCREEN_TITLE);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		
		GL11.glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		Time.setLastFrameTime(getCurrentTime());
	}
	
	public static void updateDisplay(){
		Display.sync(FPS_CAP);
		Display.update();
		long currentFrameTime = getCurrentTime();
		Time.delta = (currentFrameTime - Time.getLastFrameTime()) / 1000f;
		Time.setLastFrameTime(currentFrameTime);
	}
	
	public static float getFrameTimeSeconds() {
		return Time.delta;
	}
	
	public static void closeDisplay(){
		Display.destroy();
	}
	
	private static long getCurrentTime() {
		return Sys.getTime() * 1000 / Sys.getTimerResolution(); 
	}
	
	public static int getScreenWidth() {
		return SCREEN_WIDTH;
	}
	
	public static int getScreenHeight() {
		return SCREEN_HEIGHT;
	}
	
}
