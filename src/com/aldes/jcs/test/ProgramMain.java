package com.aldes.jcs.test;

import org.lwjgl.opengl.Display;

import com.aldes.jcs.managers.DisplayManager;

/**
 * ProgramMain
 * Demo for all JCSJava_Framework features.
 * 
 * @author JenChieh
 */
public class ProgramMain {
	
	public final static int SCREEN_WIDTH = 1280;
	public final static int SCREEN_HEIGHT = 720;
	public final static String WINDOW_TITLE = "JCSJava_Framework Demo"; 
	
	public static void main(final String[] args) {
		
		DisplayManager.createDisplay(SCREEN_WIDTH, SCREEN_HEIGHT, WINDOW_TITLE);
		
		while (!Display.isCloseRequested()) {
			
			
			
			DisplayManager.updateDisplay();
		}
		
		DisplayManager.closeDisplay();
	}
	
}
